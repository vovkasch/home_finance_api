$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "home_finance_api/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "home_finance_api"
  s.version     = HomeFinanceAPI::VERSION
  s.authors     = ["Volodymyr Shcherbyna"]
  s.email       = ["sch.icq@gmail.com"]
  #s.homepage    = "TODO"
  s.summary     = "TO DO Summary of HomeFinanceApi."
  s.description = "TO DO Description of HomeFinanceApi."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.6"
  s.add_dependency "json_api"

  s.add_development_dependency "sqlite3"
end
