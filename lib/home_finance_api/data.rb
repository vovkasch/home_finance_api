module HomeFinanceAPI
  class Data
    def self.login(email: JsonApi.email, password: JsonApi.password)
      {
        id: 1,
        service: 'sauth',
        method: 'auth',
        params: [
          {
            email: email,
            password: password
          }
        ]
      }
    end

    def self.outcome(previous_response, data)
      common previous_response, service: 'soperations',
                                method: 'write',
                                request_data: outcome_to_request_data(data)
    end

    def self.get_all_settings_data(previous_response)
      common previous_response, service: 'ssettings',
                                method: 'read',
                                request_data: [ 'ctgs', 'scheta', 'currencies', 'budgetGrid', 'userSettings', 'finTargets', 'summaryBlocks', 'commonSettings', 'userAccounts', 'accountUsers', 'accountDat' ]
    end

    def self.common(previous_response, options = {})
      {
        service: options[:service],
        method: options[:method],
        id: previous_response.request_id + 1,
        params: [
          {
            token: previous_response.token,
            idUser: previous_response.user_id.to_s,
            idAccount: previous_response.account_id.to_s
          },
          options[:request_data]
        ]
      }
    end

    def self.outcome_to_request_data(data)
      data = Array data
      request_data = data.map { |item| {
                                          operationActionType: '0',
                                          operationObject:
                                          {
                                            id: '-1',
                                            type: '0',
                                            date: item[:date].to_s,
                                            comment: item[:comment],
                                            sum: item[:amount].to_s,
                                            idCurrency: item[:currency_id].to_s,
                                            idCtg: item[:category_id].to_s,
                                            idSchet: item[:account_id].to_s
                                          }
                                        }
      }
    end
  end
end
