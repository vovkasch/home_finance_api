module HomeFinanceAPI
  class Client
    class NoConnectionException < Exception; end

    attr_reader :request, :data_object
    private :request, :data_object

    def initialize(request, data_object = Data)
      @request = request
      @data_object = data_object
    end

    def send_outcome(data)
      send { outcome data }
    end

    def fetch_all_settings
      send { get_all_settings_data }
    end

  private

    def send(&block)
      login!
      request.send block.call
      @@response = request.response
      # ResultBuilder.build @@response.send(:response_data)[:result]
    rescue SocketError => ex
      raise NoConnectionException
    end

    def login!
      if !defined?(@@response) || @@response.blank?
        request.send data_object.login
        @@response = request.response
      end
    end

    def outcome(data)
      data_object.outcome @@response, data
    end

    def get_all_settings_data
      data_object.get_all_settings_data @@response
    end
  end
end
