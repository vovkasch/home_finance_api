module HomeFinanceAPI
  autoload :Client, 'home_finance_api/client'
  autoload :Data, 'home_finance_api/data'
  autoload :ResultBuilder, 'home_finance_api/result_builder'
end
