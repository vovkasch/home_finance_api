require "#{__dir__}/test_helper"

module HomeFinanceAPI
  class ClientTest < ActiveSupport::TestCase
    class RequestMock
      attr_accessor :sent_data
      private :sent_data=

      def initialize
        @sent_data = []
        @response = [ 'login response', 'outcome response' ]
      end

      def send(data)
        @sent_data << data
      end

      def response
        @response.shift
      end
    end

    class DataMock
      def self.login
        'login data'
      end

      def self.outcome(response, data)
        'outcome data'
      end
    end





    test 'it logs in first while sending outcome' do
      Client.remove_class_variable(:@@response) if Client.class_variable_defined? :@@response

      request = RequestMock.new
      client = Client.new request, DataMock

      client.send_outcome q: 1

      assert_equal 'login data', request.sent_data[0]
    end

    test 'it sends outsome data' do
      Client.remove_class_variable(:@@response) if Client.class_variable_defined? :@@response

      request = RequestMock.new
      client = Client.new request, DataMock

      client.send_outcome q: 1

      assert_equal 'outcome data', request.sent_data[1]
    end

    test 'it sets response in the end' do
      Client.remove_class_variable(:@@response) if Client.class_variable_defined? :@@response

      request = RequestMock.new
      client = Client.new request, DataMock

      client.send_outcome q: 1

      assert_equal 'outcome response', client.class.class_variable_get(:@@response)
    end
  end
end
